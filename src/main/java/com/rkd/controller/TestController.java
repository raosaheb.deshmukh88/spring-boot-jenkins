package com.rkd.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@RequestMapping("/message")
	public String getMessage()
	{
		System.out.println("Hello...");
		return "Hello Spring Boot Jenkins........";
	}

}
